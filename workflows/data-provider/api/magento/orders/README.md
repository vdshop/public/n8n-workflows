# Data provider - API - Magento - Orders

This folder contains useful generic workflows for retrieving orders data from Magento API.

:warning: *Workflows compatibility are tested against **n8n >= 0.200**. Upgrade your instance if needed.*

## By search criteria

Retrieve (filtered) orders from Magento API, using specified search criteria.

The snippet below contains two linked nodes:
- `Set orders search criteria data`, which shows an example of available fields that can be used for pagination, sorting and filtering.
- `Search Magento API orders`, which actually performs the API calls to Magento (taking care of handling pagination) to return all matching orders.

### Usage

- Copy the snippet below (Tip: Use `Copy to clipboard` button that shows up while hovering the top right part of the snippet :arrow_down:).
- Paste it in an open n8n workflow.
- Adapt demo search criteria node to your needs.

```json
{
  "nodes": [
    {
      "parameters": {
        "source": "url",
        "workflowUrl": "https://gitlab.com/vdshop/public/n8n-workflows/-/raw/master/workflows/data-provider/api/magento/orders/search-criteria.json"
      },
      "id": "2ae95899-c41f-4a76-b872-a118ec362e19",
      "name": "Search Magento API orders",
      "type": "n8n-nodes-base.executeWorkflow",
      "position": [
        880,
        480
      ],
      "typeVersion": 1
    },
    {
      "parameters": {
        "jsCode": "return {\n  \"json\": {\n    \"page_size\": 2, // Optional field, defaults to 25,\n    \"filters\": [ // Optional field, defaults to empty array (no filtering).\n      {\n        \"field\": 'increment_id',\n        \"conditionType\": 'in',\n        \"value\": \"14000000020\",\n      }\n    ],\n    \"sort_orders\": [ // Optional field, defaults to empty array (no sorting).\n      {\n        \"field\": \"increment_id\",\n        \"direction\": \"asc\"\n      }\n    ]\n  }\n};"
      },
      "id": "15f44434-7c9f-4b9b-a767-c247458d98db",
      "name": "Set orders search criteria data",
      "type": "n8n-nodes-base.code",
      "position": [
        640,
        480
      ],
      "executeOnce": true,
      "notesInFlow": false,
      "typeVersion": 1
    }
  ],
  "connections": {
    "Set orders search criteria data": {
      "main": [
        [
          {
            "node": "Search Magento API orders",
            "type": "main",
            "index": 0
          }
        ]
      ]
    }
  }
}
```
