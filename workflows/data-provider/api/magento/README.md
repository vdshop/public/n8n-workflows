# Data provider - API - Magento

This folder contains useful generic workflows for retrieving data from Magento API.

:warning: *Workflows compatibility are tested against **n8n >= 0.200**. Upgrade your instance if needed.*

[[_TOC_]]

## By search criteria

Retrieve (filtered) entities from Magento API, using specified search criteria. Useful for any endpoint that accepts search criteria filtering.

### Details

The snippet below contains two linked nodes:
- `Set search criteria data`, which shows an example of available fields that can be used for endpoint definition, pagination, sorting and filtering.
- `Search Magento API`, which actually performs the API calls to Magento (taking care of handling pagination) to return all matching entities.

### Usage

- Copy the snippet below (Tip: Use `Copy to clipboard` button that shows up while hovering the top right part of the snippet :arrow_down:).
- Paste it in an open n8n workflow.
- Adapt `Set search criteria data` node to your needs.

```json
{
  "nodes": [
    {
      "parameters": {
        "jsCode": "return {\n  \"json\": {\n    \"endpoint\": \"products/attributes\", // Mandatory, can be any endpoint that accepts search criteria.\n    \"page_size\": 14, // Optional field, defaults to 25,\n    \"filters\": [ // Optional field, defaults to empty array (no filtering).\n      {\n        \"field\": 'frontend_input',\n        \"conditionType\": 'in',\n        \"value\": \"select,multiselect\",\n      }\n    ],\n    \"sort_orders\": [ // Optional field, defaults to empty array (no sorting).\n      {\n        \"field\": \"attribute_code\",\n        \"direction\": \"asc\"\n      }\n    ]\n  }\n};"
      },
      "id": "6dbead8f-8e4e-4408-95d3-2efe6caf22cc",
      "name": "Set search criteria data",
      "type": "n8n-nodes-base.code",
      "position": [
        -520,
        580
      ],
      "executeOnce": true,
      "notesInFlow": false,
      "typeVersion": 1
    },
    {
      "parameters": {
        "source": "url",
        "workflowUrl": "https://gitlab.com/vdshop/public/n8n-workflows/-/raw/master/workflows/data-provider/api/magento/search-criteria.json"
      },
      "id": "8be0396b-cfd5-41a7-8c0f-1ae0bdc4c15c",
      "name": "Search Magento API",
      "type": "n8n-nodes-base.executeWorkflow",
      "position": [
        -300,
        580
      ],
      "typeVersion": 1
    }
  ],
  "connections": {
    "Set search criteria data": {
      "main": [
        [
          {
            "node": "Search Magento API",
            "type": "main",
            "index": 0
          }
        ]
      ]
    }
  }
}
```
